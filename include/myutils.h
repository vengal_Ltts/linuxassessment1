#include<stdio.h>
#ifndef __MYUTILS_H
#define __MYUTILS_H
int factorial(int n);
int isPrime(int n);
int isPalindrome(int n);
int vsum(int n,...);
#endif
