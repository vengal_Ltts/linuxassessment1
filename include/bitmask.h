#ifndef __BITMASK_H
#define __BITMASK_H
int set(int n);
int reset(int n);
int flip(int n);
int query(int n);
#endif
