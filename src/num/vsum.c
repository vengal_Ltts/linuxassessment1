#include "myutils.h"
#include <stdarg.h>

int vsum(int n,...)
{
	va_list arg;
	int sum=0;
	int i;
	va_start(arg, n);
	for(i=0;i<n;i++)
	{
		sum+=va_arg(arg, int);
	}
	va_end(arg);

	return sum;
}
