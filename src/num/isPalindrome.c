#include "myutils.h"

int isPalindrome(int n)
{
	int remainder,reverse=0;
	while(n!=0)
	{
		remainder=n%10;
		reverse=reverse*10+remainder;
		n/=10;
	}
//	printf("inside%d",reverse);
	return reverse;
}
